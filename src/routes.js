import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Login from './pages/Login'
import NewDragon from './pages/NewDragon'
import Register from './pages/Register'
import Dashboard from './pages/Dashboard'
import PrivateRoute from './helpers/PrivateRoute'

export default function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Login} />
                <Route path="/register" component={Register} />
                <PrivateRoute path="/dashboard" component={Dashboard} />
                <PrivateRoute path="/dragon/new" component={NewDragon} />
            </Switch>
        </BrowserRouter>
    )
}