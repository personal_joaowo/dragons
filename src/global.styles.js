import { createGlobalStyle } from 'styled-components'

const GlobalStyles = createGlobalStyle`

    @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');

    *{
        margin: 0;
        padding: 0;
        outline: 0;
        box-sizing: border-box;
    }

    body {
        font: 400 16px Roboto, sans-serif;
        background: #f0f0f5;
        -webkit-font-somoothing: antialiased;
    }

    input, button, textarea{
        font: 400 18px Roboto, sans-serif;
    }
            
    h1 {
        font-size: 32px;
        margin-bottom: 32px;
        font-weight: 100;
    }
                    
    h2 {
        font-size: 28px;
        margin-bottom: 28px;
        font-weight: 100;
    }
                    
    h3 {
        font-size: 24px;
        margin-bottom: 24px;
        font-weight: 100;
    }
        
    a {
        color: #002F6E;

        &.back-link {
            display: block;
            margin-top: 20px;
            color: #41414d;
            font-size: 18px;
            text-decoration: none;
            font-weight: 500;
            transition: opacity 0.2s;

            svg {
                margin-right:8px;
            }

            &:hover {
                opacity: 0.8;
            }
        }
    }
    .forms-container{
        width: 100%;
        max-width: 1120px;
        height: 100vh;
        margin: 0 auto;
        display: flex;
        align-items: center;
        justify-content: space-around;

        .content {
            width: 100%;
            padding: 96px;
            background: #f0f0f5;
            box-shadow: 0 0 100px rgba( 0, 0, 0, 0.1);
            display: flex;
            justify-content: space-between;
            align-items: center;


            section {
                width: 100%;
                margin: 48px;
                .description{
                    margin-bottom: 30px;
                }
            }
            form {
                width: 100%;
            }

        }
    }

    form {
        input {
            width: 100%;
            height: 60px;
            color: #333;
            border: 1px solid #dcdce6;
            border-radius: 8px;
            padding: 0 24px;
            margin-bottom: 20px;
        }

        textarea {
            width: 100%;
            min-height: 140px;
            color: #333;
            border: 1px solid #dcdce6;
            border-radius: 8px;
            padding: 16px 24px;
            line-height: 24px;
            resize: vertical;
        }

    }

    button, .button {
        width: 100%;
        height: 60px;
        background: #041256;
        border: 0;
        border-radius: 8px;
        color: #fff;
        font-weight: 700;
        margin-top: 16px;
        text-align: center;
        text-decoration: none;
        font-size: 18px;
        line-height: 60px;
        cursor: pointer;
        
        &:hover {
            filter: brightness(90%);
        }
    }
`

export default GlobalStyles;