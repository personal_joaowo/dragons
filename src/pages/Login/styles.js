import styled from 'styled-components'

export const LoginContainer  = styled.div`
    width: 100%;
    max-width: 1120px;
    height: 100vh;
    margin: 0 auto;
    display: flex;
    align-items: center;
    justify-content: center;

    section {
        width: 100%;
        max-width: 350px;
        margin-right: 30px;
        text-align: center;
    }
`;

