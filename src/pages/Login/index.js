import React,  {useState} from 'react'
import { FiLogIn } from 'react-icons/fi'
import bigDragon from '../../assets/big-dragon.png'
import { LoginContainer } from  './styles'
import { Link, useHistory } from 'react-router-dom'

import authApi from '../../services/authApi'


function Login() {

const [userName, setName] = useState('')
const [password, setPassword] = useState('')
const history = useHistory();

  async function handleLogin(e) {
    e.preventDefault()

    const loginData = {
      userName,
      password
  }

    try{
      const response = await authApi.post('auth', loginData)     
      const { data:{ result, userName } } = response ;

      localStorage.setItem('app-token', result)
      localStorage.setItem('userName', userName)

      history.push('/dashboard')
    } catch (err) {
      console.log(err)
    }

  }


    return (
      <LoginContainer>
          <section className="form">
            <img src={bigDragon} alt="Dragons" />

            <form onSubmit={handleLogin}>
                <h1>Faça seu login</h1>
                <input 
                    type="text"
                    placeholder="Nome de Invocador"
                    value={userName}
                    onChange={ e=> setName(e.target.value)}
                />
                <input
                    type="password"
                    placeholder="Senha"
                    value={password}
                    onChange={ e=> setPassword(e.target.value)}
                />
                <button type="submit">Login</button>
                <Link className="back-link" to="/register">
                  <FiLogIn size={16} color="#041256" />
                  Ainda não sou cadastrado
                </Link>
            </form>
          </section>          
      </LoginContainer>
    );
}

export default Login