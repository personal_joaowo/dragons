import React, {useState} from 'react'
import { FiArrowLeft } from 'react-icons/fi'
import bigDragon from '../../assets/big-dragon.png'
import { Link, useHistory } from 'react-router-dom'

import authApi from '../../services/authApi'

function Register() {
    const [userName, setName] = useState('')
    const [password, setPassword] = useState('')
    const [email, setEmail] = useState('')
    const history = useHistory();

    async function handleRegister(e) {
        
        e.preventDefault();

        const data = {
            userName,
            email,
            password
        }

        try{
            const response = await authApi.post('user', data)
            history.push('/')

        } catch (err) {
            console.log(err)
        }
    }

    return (
      <div className="forms-container">
        <div className="content">
            <section>
            <img src={bigDragon} alt="Dragons" />
            <div className="description">
              <h1>Cadastro</h1>
              <p>Faça seu cadastro, e capture seus Dragões!</p>
            </div>
            <Link className="backlink" to={"/"}>
                <FiArrowLeft size={16} color="041256"/>
                Já tenho cadastro
            </Link>
            </section>

            <form onSubmit={handleRegister}>
                <input 
                    type="text"
                    placeholder="Nome de Invocador"
                    value={userName}
                    onChange={ e=> setName(e.target.value)}
                />
                <input
                    type="email"
                    placeholder="E-mail"
                    value={email}
                    onChange={ e=> setEmail(e.target.value)}
                />
                <input
                    type="password"
                    placeholder="Senha"
                    value={password}
                    onChange={ e=> setPassword(e.target.value)}
                />
                
                <button className="button" type="submit">
                    Cadastrar
                </button>
            </form>
        </div>
      </div>
      
    );
}

export default Register