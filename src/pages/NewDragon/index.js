import React, {useState, useEffect} from 'react'
import { FiArrowLeft } from 'react-icons/fi'
import bigDragon from '../../assets/big-dragon2.png'
import { Link, useHistory } from 'react-router-dom'
import api from '../../services/api'

function NewDragon() {

    const [name, setName] = useState('')
    const [type, setType] = useState('')
    const [histories, setHistories] = useState([])

    const [historiesField, setHistoriesField] = useState([])

    const history = useHistory();

    async function handleNewDragon(e) {

        e.preventDefault()

        const dragonData = {
            name,
            type,
            histories: Object.values(histories)
        }

        try {
            await api.post('', dragonData)    
            history.push('/dashboard')
        } catch (err) {
            console.log(err)
        }
    }

    return (
      <div className="forms-container">
        <div className="content">
            <section>
                <div className="description">
                <h1>Sala de Invocação</h1>
                <p>Descreva no formulário as caractéristicas do Dragão que você deseja.</p>
                </div>

                <img src={bigDragon} alt="Dragons" />

                <br />
                <Link className="backlink" to={"/dashboard"}>
                    <FiArrowLeft size={16} color="041256"/>
                    Voltar para pagina inicial
                </Link>
            </section>
            <form onSubmit={handleNewDragon}>
                <input 
                    type="text"
                    placeholder="Nome do Dragão"
                    name="name"
                    value={name}
                    onChange={e => setName(e.target.value)}
                />
                <input 
                    type="text"
                    placeholder="Tipo do Dragão"
                    name="type"
                    value={type}
                    onChange={e => setType(e.target.value)}
                />
                {historiesField.map((field, i) => {
                    return (<input key={i}
                    name={`${field}-${i}`}
                    type="text"
                    placeholder={`História ${i + 1}` }
                    onChange={e => setHistories({ ...histories, [e.target.name] : e.target.value })}
                    />)
                })}
                    
                <button type="button"
                onClick={() => setHistoriesField([...historiesField,'history'])}>
                    Adicionar história
                </button>

                <button className="button" type="submit">
                    Invocar
                </button>
            </form>
        </div>
      </div>
      
    );
}

export default NewDragon