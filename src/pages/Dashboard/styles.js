import styled from 'styled-components'

export const DashboardContainer  = styled.div`
    width: 100%;
    max-width: 1180px;
    padding: 0 30px;
    height: 100vh;
    margin: 32px auto;
    
    header {
        display: flex;
        align-items: center;

        span {
            font-size: 20px;
            margin-left: 24px;
        }

        a {
            width: 260px;
            margin-left: auto;
            margin-top:0;
        }
        
        button {
            height:60px;
            width: 60px;
            margin-top:0;
            margin-left: 20px;
            border: 1px solid #dcdce6;
            background: #c1d1f1;
            transition: border-color 0.2s;
        }

    }

    h1 {
        margin-top: 80px;
        margin-bottom: 24px;
    }

    .grid-list {
        display: grid;
        grid-template-columns: 1fr 1fr;
        grid-gap: 24px;
        list-style: none;

        >li {
            background: #fff;
            padding: 24px;
            border-radius: 8px;
            position: relative;
            min-height: 210px;

            h2, h3, ul {
                max-width: 80%;
            }

            strong {
                display: block;
                margin-bottom: 16px;
            }

            p{
                color: #737380;
                line-height: 21px;
            }

            ul {
                list-style: lower-roman;
                padding: 0 0 24px 32px;
                
                li{
                    padding: 0;
                    margin-bottom: 5px;
                }
            }

            .floating-buttons{
                position: absolute;
                right: 24px;
                top: 24px;

                button{
                    border: 0;
                    line-height: 100%;
                    
                    &:hover{
                        opacity: 0.8
                    }
                }
            }
        }
    }

`;

