import React,  { useEffect, useState } from 'react'
import { FiEdit, FiPower, FiTrash } from 'react-icons/fi'
import Dragon from '../../assets/dragon.png'
import {  DashboardContainer } from  './styles'
import { Link, useHistory } from 'react-router-dom'

import api from '../../services/api'

function Dashboard () {
    const userName = localStorage.getItem('userName')
    const [dragons, setDragons] = useState([])
    const history = useHistory()

    useEffect(() => {
        api.get().then(response => {
            setDragons(response.data)
        })
    }, [userName])

    async function handleDeleteDragon(id) {
        try {
            await api.delete(id)
            setDragons(dragons.filter( dragon => dragon.id !== id ))
        } catch (err) {
            alert(err);
        }
    }

    function handleLogout() {
      localStorage.clear()
      history.push('/');
    }
    return(
        <DashboardContainer>

            <header>
                <img src={Dragon} alt="Dragons"/>
                <span>Bem vindo {userName}</span>

                <Link className="button" to="dragon/new">
                    Invocar Dragão
                </Link>
                <button  onClick={handleLogout}>
                    <FiPower size={18} color="@041256" />
                </button>
            </header>
            
            <h1>Dragões Invocados</h1>

            <ul className="grid-list">
                {dragons.map( dragon => (
                    <li key={dragon.id}>
                        <h2>{dragon.name}</h2>
                        
                        {dragon.histories.length > 0 && 
                        (<>
                            <h3>Histórias</h3>
                            <ul>
                                {dragon.histories.map( (history, i) => (
                                    <li key={i}>{history}</li>
                                ))}
                            </ul>
                        </>
                        )}
                        <div className="floating-buttons">
                            <button type="button">
                                <FiEdit size={32} />
                            </button>
                            <button onClick={() => handleDeleteDragon(dragon.id)} type="button">
                                <FiTrash size={32} />
                            </button>
                        </div>
                    </li>
                ))}
                

            </ul>


        </DashboardContainer>
    )
}

export default Dashboard